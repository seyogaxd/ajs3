const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
  };
  
  const newEmployee = {
    ...employee,
    age: 47,
    salary: 100000
  };
  
  console.log(newEmployee);
  